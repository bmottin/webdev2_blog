# Commande Symfony

## Installation repo
git clone nom_repo 
cd nom_repo
composer update

## Serveur

demarrer le serveur sans check les certificats

`symfony server:start --no-tls` 

arreter le serveur 

`symfony server:stop` 

## Formulaire
creer un nouveau formulaire
`php bin/console make:form`
choisir l'entité
adapter les champs

## Controller

creer un nouveau controller 

`php bin/console make:controller`

## Doctrine / BDD

php bin/console doctrine:database:create

php bin/console make:migration

php bin/console doctrine:migration:migrate

### ajouter Fixtures sur le projet

`composer require --dev orm-fixtures`

### Insere les fixtures dans la BDD

`php bin/console doctrine:fixtures:load`

Mettre à jour la version de PHP

`symfony local:php:refresh`

### hasher un password avec Symfony 
symfony console security:hash-password


## CKEditor 

install 
`composer require friendsofsymfony/ckeditor-bundle`

`php bin/console ckeditor:install`

deplace les fichiers dans public
`php bin/console assets:install public`

### usage
use FOS\CKEditorBundle\Form\Type\CKEditorType;

CKEditorType::class

# LiipImageBundle
## présentation 
Optimisation des images et ajout d'un watermark

## installation
composer require liip/imagine-bundle

## CRUD
pour la création du BO:
    - taper la commande `php bin/console make:crud`
    - choisir l'entité à gerer
    - je conseille de changer le nom du controller en AdminClassController
    - changer le template
    

# TWIG 

~ sert à la concatenation

# MAILDEV

## installation local

npm install maildev

## Usage

npx maildev --hide-extension STARTTLS


-----

# Deploiement

dump BDD
Mise à jour du .env pour server distant/cible

cette commande extrait la configuratiuon et est lisible par apache
composer dump-env prod
cela genere un fichier .env.local.php

ensuite la commande ci-dessous va installer les dépendances composer, mais que ceux de prod
composer install --no-dev --optimize-autoloader

envoyer via FTP:
- config
- src
- templates
- public
- node_modules
- vendor
- .env.local.php
- composer.json
- composer.lock
- symfony.lock
- package.json
- package-lock.json

une fois le transfert terminé, on envoie le .htaccess_root_https à la racine
et dans le .htaccess_public dans le dossier public
et après on renomme les 2 fichiers .htaccess

That's all folks!!!