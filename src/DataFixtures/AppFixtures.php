<?php

namespace App\DataFixtures;

use App\Controller\SecurityController;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserProfile;
use Container3ZDXXoT\getSluggerService;
use ContainerCNKXzu8\getSluggerService as ContainerCNKXzu8GetSluggerService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $hasher;
    private SluggerInterface $slugger;
    function __construct(SluggerInterface $slugger, UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {

        /*
            Status
        */
        $sta1 = new Status();
        $sta1->setLabel('Publié');
        $sta1->setInternalName('published');
        $sta1->setSortOrder(3);

        $sta2 = new Status();
        $sta2->setLabel('Brouillon');
        $sta2->setInternalName('draft');
        $sta2->setSortOrder(1);

        $sta3 = new Status();
        $sta3->setLabel('En attente');
        $sta3->setInternalName('pending');
        $sta3->setSortOrder(2);

        $manager->persist($sta1);
        $manager->persist($sta2);
        $manager->persist($sta3);

        $categ1 = new Category();
        $categ1->setLabel("IT");

        $categ2 = new Category();
        $categ2->setLabel("Jeux Video");

        $categ3 = new Category();
        $categ3->setLabel("Marketing");

        $manager->persist($categ1);
        $manager->persist($categ2);
        $manager->persist($categ3);

      
        $contentArticle = [
            [
                "title" => "Comment generer de l'argent ?",
                "content" => "Il existe plusieurs régies publicitaires que vous pouvez utiliser pour placer des annonces sur un site web. Voici quelques exemples courants:
        Google AdSense: C'est l'une des régies publicitaires les plus populaires qui vous permet de diffuser des annonces contextuelles sur votre site web. Les annonces sont sélectionnées en fonction du contenu de votre site web et de vos visiteurs.
                AdThrive: C'est une régie publicitaire qui offre des solutions pour les éditeurs de contenu, comme les blogs et les sites web de niche. Ils offrent une variété de formats d'annonces, y compris des annonces native, des annonces vidéo et des annonces d'affichage.
                Media.net: une régie publicitaire qui propose des annonces contextuelles, basées sur le contenu, pour les éditeurs de contenu, les blogs et les sites web de niche.
                Taboola & Outbrain: Ces régies publicitaires permettent de diffuser des annonces de contenu recommandé, qui apparaissent généralement en bas des articles de blog ou des pages de contenu.
                Infolinks: une régie publicitaire qui permet de placer des liens contextuels dans le contenu de votre site web.
                Amazon Associates : C'est un programme d'affiliation qui vous permet de placer des liens d'affiliation Amazon sur votre site web et de gagner de l'argent en fonction des ventes réalisées à travers ces liens.
            Il est important de noter que chaque régie publicitaire a ses propres exigences pour les éditeurs, donc il est important de vérifier les termes et les conditions avant de vous inscrire.",
                "image" => "",
                "category" => $categ2
            ],
            [
                "title" => "Présentation de Symfony",
                "content" => "Symfony est un framework PHP populaire pour le développement d'applications web. Il suit le modèle de conception MVC (Modèle-Vue-Contrôleur) et offre une architecture modulaire pour le développement de projets de toutes tailles.
<p>Il est basé sur des composants réutilisables, ce qui facilite la création de fonctionnalités spécifiques à votre application. Les composants incluent la gestion des requêtes HTTP, la sécurité, la validation de formulaires, et bien plus encore.</p>
<p>Symfony est également connu pour son écosystème riche, qui comprend une grande communauté, une documentation exhaustive et des outils pour le développement, le débogage et le déploiement d'applications.</p>
<p>Il est compatible avec la plupart des bases de données relationnelles, telles que MySQL, PostgreSQL et SQLite, et prend en charge de nombreuses fonctionnalités avancées telles que la gestion de versions, les tests automatisés et les services web RESTful.</p>
<p>Le processus de développement de Symfony est guidé par les meilleures pratiques de l'industrie et les normes de codage, ce qui garantit une qualité de code élevée et une maintenance facilitée.</p>
<p>Symfony est utilisé par de grandes entreprises telles que Spotify, Drupal, et l'UNESCO, ainsi que par de nombreux développeurs indépendants pour créer des applications web robustes et évolutives.</p>
<p>Enfin, Symfony est disponible sous licence open-source et peut être téléchargé gratuitement à partir de son site officiel. Les mises à jour régulières et la communauté active font de Symfony une solution viable pour le développement d'applications web modernes.</p>",
                "image" => "logo_symf.png",
                "category" => $categ1

            ],
            [
                "title" => "C'est quoi Ubuntu ?",
                "content" => "<p>Ubuntu est un système d'exploitation basé sur Linux, conçu pour une utilisation sur des ordinateurs de bureau, des serveurs, des tablettes et des smartphones.</p>
        <p>Il est développé par la communauté open-source et soutenu par Canonical, une entreprise britannique qui fournit des services et des outils pour Ubuntu.</p>
        <p>Ubuntu est connu pour sa simplicité d'utilisation et sa facilité d'installation. Il est livré avec une grande variété de logiciels pré-installés, notamment un navigateur web, un client de messagerie, un lecteur multimédia, un traitement de texte et bien plus encore.</p>
        <p>Ubuntu est également célèbre pour son centre de logiciels, qui permet aux utilisateurs de télécharger et d'installer facilement des applications supplémentaires, avec un système de gestion des dépendances automatisé.</p>
        <p>Il est compatible avec une large gamme de matériel, y compris les ordinateurs portables, les ordinateurs de bureau et les serveurs, ainsi que les plateformes de cloud computing telles que Amazon Web Services et Microsoft Azure.</p>
        <p>Ubuntu est basé sur Debian, un système d'exploitation également basé sur Linux, et utilise le gestionnaire de paquets APT (Advanced Packaging Tool) pour la gestion des logiciels.</p>
        <p>Il est régulièrement mis à jour avec des versions majeures publiées tous les deux ans, avec des mises à jour de sécurité et de maintenance disponibles tous les mois.</p>
        <p>Ubuntu est également connu pour son engagement envers l'open-source et ses communautés actives de développeurs et d'utilisateurs qui travaillent ensemble pour améliorer le système d'exploitation et créer des applications.</p>
        <p>Enfin, Ubuntu est disponible gratuitement en téléchargement à partir de son site officiel, ce qui en fait une alternative viable aux systèmes d'exploitation propriétaires tels que Windows et macOS.</p>",

                "image" => "logo_ubuntu.png",
                "category" => $categ1

            ],

            [
                "title" => "C'est quoi le noyau linux ?",
                "content" => "<p>Le noyau Linux est la partie centrale du système d'exploitation Linux. Il est responsable de la gestion des ressources matérielles telles que le processeur, la mémoire, les périphériques de stockage et les interfaces réseau, ainsi que de la fourniture d'une interface entre les applications et le matériel.</p>
        <p>Le noyau Linux a été créé en 1991 par Linus Torvalds, un étudiant finlandais qui cherchait à créer un système d'exploitation gratuit et open-source pour les ordinateurs personnels. Depuis lors, le noyau Linux a connu un développement continu par une communauté mondiale de développeurs, qui travaillent ensemble pour ajouter de nouvelles fonctionnalités, corriger les bugs et améliorer les performances.</p>
        <p>Le noyau Linux est distribué sous licence open-source, ce qui signifie qu'il est disponible gratuitement pour tous ceux qui souhaitent l'utiliser, le modifier et le distribuer. Cela a conduit à l'émergence de nombreuses distributions Linux différentes, qui combinent le noyau Linux avec des outils et des applications supplémentaires pour créer des systèmes d'exploitation complets.</p>
        <p>Le noyau Linux est également connu pour sa stabilité, sa sécurité et sa modularité. Il peut être configuré pour prendre en charge une large gamme de matériels et de configurations système différents, ce qui le rend adapté à une grande variété d'applications, allant des ordinateurs personnels aux serveurs en passant par les systèmes embarqués tels que les routeurs et les dispositifs IoT.</p>
        <p>En résumé, le noyau Linux est le cœur du système d'exploitation Linux, responsable de la gestion des ressources matérielles et de la fourniture d'une interface entre les applications et le matériel. Il est distribué sous licence open-source et est connu pour sa stabilité, sa sécurité et sa modularité.</p>
        ",

                "image" => "logo_tux.png",
                "category" => $categ1
            ],
            [
                "title" => "Ça correspond à quoi le 8-bit ?",
                "content" => "<p>Le terme 8-bit se réfère à la taille du bus de données utilisé par le processeur central dans les anciennes consoles de jeux vidéo. Plus précisément, cela signifie que le processeur pouvait traiter des données en paquets de 8 bits à la fois.

        Les consoles de jeux vidéo 8 bits les plus connues sont la Nintendo Entertainment System (NES), la Sega Master System et l'Atari 7800. Ces consoles ont été commercialisées dans les années 1980 et au début des années 1990 et ont été remplacées par des consoles de jeux plus avancées, telles que les consoles 16 bits et 32 bits.</p>
        ",

                "image" => "photo_nes.png",
                "category" => $categ2
            ],
            [
                "title" => "Neo-Geo",
                "content" => "<p>La Neo Geo est une console de jeux vidéo développée par la société japonaise SNK. Elle est sortie en 1990 sous la forme d'une borne d'arcade, et a ensuite été adaptée en une console de salon, la Neo Geo AES (Advanced Entertainment System), sortie en 1991.

        Voici les principales spécifications techniques de la Neo Geo :
        <ul>
            <li>Processeur principal : Motorola 68000 à 12 MHz</li>
            <li>Processeur audio : Zilog Z80 à 4 MHz</li>
            <li>RAM : 64 Ko</li>
            <li>ROM : 2 Ko (pour le BIOS)</li>
            <li>Couleurs : 4096 (palette de 65536 couleurs)</li>
            <li>Résolution d'affichage : 320x224 pixels</li>
            <li>Capacités sonores : 4 canaux stéréo PCM (Pulse Code Modulation)</li>
            <li>Supports de stockage : cartouches ROM (jusqu'à 330 Mo)</li>
            <li>Ports d'entrée/sortie : 2 ports manettes, 1 port vidéo composite, 1 port stéréo RCA, 1 port casque</li>
        </ul>
        La Neo Geo était considérée comme une console de jeux vidéo très avancée pour son époque, notamment grâce à ses capacités graphiques et sonores avancées, ainsi qu'à sa grande capacité de stockage grâce aux cartouches ROM. Elle était capable de produire des jeux aux graphismes colorés et détaillés, avec des animations fluides et un son de qualité.</p>
        ",

                "image" => "photo_neo.png",
                "category" => $categ2
            ],

        ];

        $comments = [
            "Yes trop bien",
            "Tip top",
            "C'est nul",
            "C'est interessant",
            "Boooh",
            "C'est trop dare !!!",
            "mouais bof",
            "C'est une bonne situation ça commentaire? Oui bien s^r Commentaire est un très bonne situation, pour la famille et même pour soit. Je sais que cela peut être ^parfois difficile à lre, mais je pense que ça eut être cool ^^",
            "Boom 🧨",
            "On va Coder 🐷",
            "Du moment que ça marche",
        ];

        $comAuthors = [
            "Benoit",
            "Bilbo the Robbits",
            "Robby the Robots",
            "Bender B. Rodriguez",
            "Helen Replay",
            "William SAURON",
        ];

        $user1 = new User();
        $user1->setRoles(['ROLE_USER']);
        $user1->setEmail("u1@gmail.com");
        // admin123
        // $user1->setPassword('$2y$13$y1hKdeF9lhoULnFVS5v3s.MzWz.rZ4gRsRWLCsOj.chTpNY0LteCa');
        $user1->setPassword($this->hasher->hashPassword($user1, "admin123"));
        $user1->setUsername("John DOE");
        $user1->setIsValidate(true);
        // appel methode static ::
        $user1->setValidationCode(SecurityController::generateValidationCode());
        $user1->setHasRGPD(true);
        $user1->setRgpdValidationDate(new \DateTimeImmutable());
        $user1->setDateUpdatedAt(new \DateTimeImmutable());
        $manager->persist($user1);

        $user2 = new User();
        $user2->setRoles(['ROLE_USER', 'ROLE_AUTHOR']);
        $user2->setEmail("u2@gmail.com");
        // $user2->setPassword('$2y$13$y1hKdeF9lhoULnFVS5v3s.MzWz.rZ4gRsRWLCsOj.chTpNY0LteCa');
        $user2->setPassword($this->hasher->hashPassword($user2, "admin123"));
        $user2->setUsername("Jane DOE");
        $user2->setIsValidate(true);
        // appel methode static ::
        $user2->setValidationCode(SecurityController::generateValidationCode());
        $user2->setHasRGPD(true);
        $user2->setRgpdValidationDate(new \DateTimeImmutable());
        $user2->setDateUpdatedAt(new \DateTimeImmutable());
        $manager->persist($user2);

        $user3 = new User();
        $user3->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $user3->setEmail("a1@gmail.com");
        //$user3->setPassword('$2y$13$y1hKdeF9lhoULnFVS5v3s.MzWz.rZ4gRsRWLCsOj.chTpNY0LteCa');
        $user3->setPassword($this->hasher->hashPassword($user3, "admin123"));
        $user3->setUsername("John WICK");
        $user3->setIsValidate(true);
        // appel methode static ::
        $user3->setValidationCode(SecurityController::generateValidationCode());
        $user3->setHasRGPD(true);
        $user3->setRgpdValidationDate(new \DateTimeImmutable());
        $user3->setDateUpdatedAt(new \DateTimeImmutable());
        $manager->persist($user3);

        $users = [$user2,$user3];

        /*
         * USER PRofile
         * */
        $userPro1 = new UserProfile();
        $userPro1->setUser($user1);
        $userPro1->setBiography("C'est la bio d'U1");
        $userPro1->setAvatar("image.png");
        $manager->persist($userPro1);

        $userPro2 = new UserProfile();
        $userPro2->setUser($user2);
        $userPro2->setBiography("C'est la bio d'U2");
        $userPro2->setAvatar("image2.png");
        $manager->persist($userPro2);

        $userPro3 = new UserProfile();
        $userPro3->setUser($user3);
        $userPro3->setBiography("C'est la bio d'U3");
        $userPro3->setAvatar("image.png");
        $manager->persist($userPro3);

        /*
         █████╗ ██████╗ ████████╗██╗ ██████╗██╗     ███████╗
        ██╔══██╗██╔══██╗╚══██╔══╝██║██╔════╝██║     ██╔════╝
        ███████║██████╔╝   ██║   ██║██║     ██║     █████╗  
        ██╔══██║██╔══██╗   ██║   ██║██║     ██║     ██╔══╝  
        ██║  ██║██║  ██║   ██║   ██║╚██████╗███████╗███████╗
        ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝╚══════╝╚══════╝
        */

        $statuses = [
            $sta1,
            $sta2,
            $sta3,
        ];

        foreach ($contentArticle as $key => $art) {
            $article = new Article();
            $article->setTitle($art['title']);
            $article->setContent($art['content']);
            $article->setImage($art['image']);
            $article->setCategory($art['category']);
            $article->setAuthor($users[rand(0, count($users)-1)]);
            $article->setSlug($this->slugger->slug($article->getTitle()));
            $article->setDateCreated(new \DateTimeImmutable());
            $article->setDateUpdated(new \DateTimeImmutable());
            $article->setStatus($statuses[rand(0, count($statuses)-1)]);

            $manager->persist($article);

            // nouvelle instance de commentaire
            for ($i = 0; $i < rand(0, 12); $i++) {

                $com = new Comment();
                $com->setAuthor($comAuthors[rand(0, count($comAuthors) - 1)]);
                $com->setContent($comments[rand(0, count($comments) - 1)]);
                $com->setArticle($article);
                $com->setDateCreated(new \DateTimeImmutable());
                // je l'enregistre
                $manager->persist($com);
            }
        }

        $manager->flush();
    }
}
