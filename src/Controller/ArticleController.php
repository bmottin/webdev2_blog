<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\StatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleController extends AbstractController
{
    #[Route('/article', name: 'app_article')]
    public function appArticle(ArticleRepository $articleRepository,StatusRepository $statusRepository): Response
    {
        $status = $statusRepository->findOneBy(['internalName' => 'published']);
        // dump($articleRepository->findAll());
        return $this->render('article/articles.html.twig', [
            // "articles" => $articleRepository->findAll()
            // "articles" => $articleRepository->findBy(['status' => $status])
            "articles" => $articleRepository->findByStatusBis('published')
        ]);
    }

    #[Route('/article/{slug}/', name: 'app_article_show_slug')]
    // public function appArticleShow(ArticleRepository $articleRepository, $id): Response
    public function appArticleShowSlug(Article $article, Request $request, EntityManagerInterface $entityManager): Response // injection de dépendance
    {
        $comment = new Comment();
        $form = $this->createFormBuilder($comment)
            ->add("author")
            ->add("content")
            ->add("submit", SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setArticle($article);
            $comment->setDateCreated(new \DateTimeImmutable());

            $entityManager->persist($comment);
            $entityManager->flush();
            $this->addFlash("success", "Merci pour votre commentaire");
            /*$comment->setAuthor("");
            $comment->setContent("");//*/
            return $this->redirectToRoute('app_article_show', ['id' => $article->getId()]);
        }

        return $this->render('article/article.html.twig', [
            "article" => $article,
            "commentForm" => $form->createView()
        ]);
    }

    #[Route('/article/{id}/show', name: 'app_article_show')]
    // public function appArticleShow(ArticleRepository $articleRepository, $id): Response
    public function appArticleShow(Article $article, Request $request, EntityManagerInterface $entityManager): Response // injection de dépendance
    {
        $comment = new Comment();
        $form = $this->createFormBuilder($comment)
            ->add("author")
            ->add("content")
            ->add("submit", SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setArticle($article);
            $comment->setDateCreated(new \DateTimeImmutable());

            $entityManager->persist($comment);
            $entityManager->flush();
            $this->addFlash("success", "Merci pour votre commentaire");
            /*$comment->setAuthor("");
            $comment->setContent("");//*/
            return $this->redirectToRoute('app_article_show', ['id' => $article->getId()]);
        }

        // $article = $articleRepository->findOneBy(['id' => $id]);
        // dump($article);
        return $this->render('article/article.html.twig', [
            "article" => $article,
            "commentForm" => $form->createView()
        ]);
    }

    #[Security("is_granted('ROLE_AUTHOR')")]
    #[Route('/article/new', name: 'app_article_new')]
    public function appArticleNew(Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response // injection de dépendance
    {
        // instance vide d'Article
        $article = new Article();
        // creation formulaire
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'class' => 'form-control',
                    "style" => "background-color: lightblue"

                ]
            ])
            ->add('author', EntityType::class, [
                'class' => User::class,
                // uses the User.username property as the visible option string
                'choice_label' => 'username',
                'expanded' => false, // si expanded, case à cocher/radio
                'multiple' => false, // si multiple checkbox
            ])
            ->add('content')
            ->add('image')
            ->add('category')
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setSlug($slugger->slug($article->getTitle()));
            $article->setDateCreated(new \DateTimeImmutable())
                ->setDateUpdated(new \DateTimeImmutable());
            $entityManager->persist($article);
            $entityManager->flush();
            $this->addFlash("success", "Article ajouté avec succès");
            return $this->redirectToRoute('app_article_show', ['id' => $article->getId()]);
        }
        return $this->render('article/new.html.twig', [
            'articleForm' => $form->createView()
        ]);
    }

    #[Security("is_granted('ROLE_AUTHOR')")] // check son rôle et s'il est connecté
    #[Route('/article/{id}/edit', name: 'app_article_edit')]
    public function appArticleEdit(Article $article, Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response // injection de dépendance
    {
        if($this->getUser() != $article->getAuthor()) {
            // afficher un message d'erreur
            $this->addFlash('warning', "Vous n'êtes l'auteur de cette article");
            // rediriger vers page accueil
            return $this->redirectToRoute('app_main');
        }
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setSlug($slugger->slug($article->getTitle()));
            $article->setDateUpdated(new \DateTimeImmutable());

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash("success", "Article mis à jour avec succès");

            return $this->redirectToRoute('app_article_show', ['id' => $article->getId()]);
        }

        return $this->render('article/edit.html.twig', [
            "article" => $article,
            "form" => $form->createView()
        ]);
    }


    #[Route('/article/new1', name: 'app_article_new1')]
    public function appArticleNew1(Request $request, EntityManagerInterface $entityManager): Response // injection de dépendance
    {
        $article = new Article();
        // si la request de type POST est envoyée, et qu'il y a des datas
        if ($request->request->count() > 0) {
            // je peuple mon objet
            $article->setTitle($request->request->get("title"))
                ->setContent($request->request->get("content"))
                ->setImage($request->request->get("image"))
                ->setDateCreated(new \DateTimeImmutable())
                ->setDateUpdated(new \DateTimeImmutable());
            // persist va ajouter l'objet en liste d'attente d'insertion dans la BDD
            $entityManager->persist($article);
            // Flush declenche l'insertion
            $entityManager->flush();
            return $this->redirectToRoute('app_article_show', ['id' => $article->getId()]);
        }
        // $article = $articleRepository->findOneBy(['id' => $id]);
        // dump($request);
        return $this->render('article/new1.html.twig', [
            "article" => $article
        ]);
    }
}
