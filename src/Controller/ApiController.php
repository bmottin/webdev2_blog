<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    #[Route('/api/getArticles', name: 'app_api')]
    public function getArticles(ArticleRepository $articleRepository): JsonResponse {

        $art = $articleRepository->find(1);
        // dump($art);
        $res = [];
        
        return new JsonResponse(json_encode($articleRepository->find(1)));
    }
}
