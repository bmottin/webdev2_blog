<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[Route('/mail')]
class MailController extends AbstractController
{

    // FIXME env

    private $mailer = null;
    private $from = "contact@monblog.com";

    public function __construct()
    {
        $mailer_dsn = "smtp://localhost:1025";
        $transport = Transport::fromDsn($mailer_dsn);
        $this->mailer = new Mailer($transport);
    }

    #[Route('/test', name: 'app_mail')]
    public function index(): Response
    {
       

        $email = (new Email())
            ->from($this->from)
            ->to('client@client.com')
            ->subject('Mail Test')
            ->text("Ceci est un mail de test")
            ->html("<h1>Test</h1>
            <p>
                Ceci est un mail de test, mais en <strong>HTML</strong>
            </p>");//*/
           

        $this->mailer->send($email);
        // dd($mailer);
        return new Response('Mail envoyé', Response::HTTP_ACCEPTED);
    }

    #[Route('/send-register-code/{mail}', name: 'app_mail_send_register_code')]
    public function sendRegisterCode($mail, EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email' => $mail]);
        if ($user) {
            
            // MAJ du code de validation
            $user->setValidationCode(SecurityController::generateValidationCode());
            $user->setDateUpdatedAt(new \DateTimeImmutable());

            $manager->persist($user);
            $manager->flush();
            //*/
            // generation d'une URL
            $confirm_url = $this->generateUrl('app_security_validation_code', ['code' => $user->getValidationCode()], UrlGeneratorInterface::ABSOLUTE_URL);



            $body = $this->renderView('mail/security.html.twig', [
                'text' => "<h1>Test</h1>
                <p>
                Bienvenue {$user->getUsername()}, voici le lien de validation de votre inscription <a href='{$confirm_url}'>ici</a>.
                <br>
                si votre client mail ne prend pas en charge l'HTML, copier coller le lien ci-dessous: <br>
                {$confirm_url}.
                </p>",
            ]);

            $email = (new Email())
                ->from($this->from)
                ->to($user->getEmail())
                ->subject('Inscription sur monblog.com')
                ->text("Bienvenue {$user->getUsername()}, voici le lien de validation de votre inscription : {$confirm_url}")
                // ->html();
            ->html($body);
            // essayer de faire un traitement
            try {
                $this->mailer->send($email);
            // } catch (\Throwable $th) {
            } catch (\Exception $th) {
                // Exception c'est une erreur
                // catch permet "d'attraper" l'erreur et la traité

                // dd($th);
                //throw $th;
                $this->addFlash('danger',"{$th->getMessage()}<br> Veuillez contacter l'administrateur avec le code erreur suivant : {$th->getCode()}");
                return $this->redirectToRoute('app_main');
            }
            
            // dd($mailer);
        } else {
            $this->addFlash('danger', 'Une erreur est survenue, contacter l\'admin');
            return $this->redirectToRoute('app_main');
        }
        //return new Response('Mail envoyé', Response::HTTP_ACCEPTED);
    }

    #[Route('/send-recovery-password/{mail}', name: 'app_mail_send_recovery_password')]
    public function sendRecoveryPassword($mail, EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email' => $mail]);
        if ($user) {
            $url = $this->generateUrl("app_security_change_password",["validationCode" => $user->getValidationCode()],UrlGeneratorInterface::ABSOLUTE_URL);

            $email = new Email();
            $email->from($this->from)
                  ->to($user->getEmail())
                  ->subject('Demande de récuperation de mot de passe - MonBlog')
                  ->text("Bonjour {$url}")
                  ->html("bonjour {$url}");
                  
            $this->mailer->send($email);
        }



        return new Response('Mail envoyé', Response::HTTP_ACCEPTED);
    }
}
