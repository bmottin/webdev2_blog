<?php

namespace App\Controller;

use App\Entity\UserProfile;
use App\Form\UserProfileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserProfileController extends AbstractController
{
    #[Route('/userprofile/{id}', name: 'app_user_show_profile')]
    // #[Route('/user/profile/{nickname}', name: 'app_user_show_profile')]
    public function showUserProfile(UserProfile $userProfile): Response
    {
        return $this->render('user_profile/show.html.twig', [
            'userProfile' => $userProfile,
        ]);
    }


    #[Route('/user/profile/{id}/edit', name: 'app_user_edit_profile')]
    // #[Route('/user/profile/{nickname}', name: 'app_user_show_profile')]
    public function editUserProfile(UserProfile $userProfile, Request $request, EntityManagerInterface $manager): Response
    {
        
        $form = $this->createForm(UserProfileType::class, $userProfile);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $userProfile->getUser()->setDateUpdatedAt(new \DateTimeImmutable());
            $userProfile->setDateUpdatedAt(new \DateTimeImmutable());
            
            $manager->persist($userProfile);
            $manager->flush();
            $this->addFlash('success', "Votre profil a été mis à jour. ");
            return $this->redirectToRoute('app_user_show_profile', ['id' => $userProfile->getId()]);
        }
        
        return $this->render('user_profile/edit.html.twig', [
            'form' => $form->createView(),
            'userProfile' => $userProfile,
           
        ]);
    }
}
