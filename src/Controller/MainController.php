<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_main')]
    public function appMain(): Response
    {
        return $this->render('main/index.html.twig', [
            "age" => 20
        ]);
    }

    #[Route('/cgu', name: 'app_cgu')]
    public function appCgu(): Response
    {
        return $this->render('main/cgu.html.twig', [
            "isTLDR" => false
        ]);
    }
}
