<?php

namespace App\Controller;

use App\Entity\Tagword;
use App\Form\TagwordType;
use App\Repository\TagwordRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/tagword')]
class AdminTagwordController extends AbstractController
{
    #[Route('/', name: 'app_admin_tagword_index', methods: ['GET'])]
    public function index(TagwordRepository $tagwordRepository): Response
    {
        return $this->render('admin_tagword/index.html.twig', [
            'tagwords' => $tagwordRepository->findAll(),
            'active' => 'tagword'
        ]);
    }

    #[Route('/new', name: 'app_admin_tagword_new', methods: ['GET', 'POST'])]
    public function new(Request $request, TagwordRepository $tagwordRepository): Response
    {
        $tagword = new Tagword();
        $form = $this->createForm(TagwordType::class, $tagword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tagwordRepository->save($tagword, true);

            return $this->redirectToRoute('app_admin_tagword_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_tagword/new.html.twig', [
            'tagword' => $tagword,
            'form' => $form->createView(),
            'active' => 'tagword'
        ]);
    }

    #[Route('/{id}', name: 'app_admin_tagword_show', methods: ['GET'])]
    public function show(Tagword $tagword): Response
    {
        return $this->render('admin_tagword/show.html.twig', [
            'tagword' => $tagword,
            'active' => 'tagword'
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_tagword_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Tagword $tagword, TagwordRepository $tagwordRepository): Response
    {
        $form = $this->createForm(TagwordType::class, $tagword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tagwordRepository->save($tagword, true);

            return $this->redirectToRoute('app_admin_tagword_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_tagword/edit.html.twig', [
            'tagword' => $tagword,
            'form' => $form->createView(),
            'active' => 'tagword'
        ]);
    }

    #[Route('/{id}', name: 'app_admin_tagword_delete', methods: ['POST'])]
    public function delete(Request $request, Tagword $tagword, TagwordRepository $tagwordRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tagword->getId(), $request->request->get('_token'))) {
            $tagwordRepository->remove($tagword, true);
        }

        return $this->redirectToRoute('app_admin_tagword_index', [], Response::HTTP_SEE_OTHER);
    }
}
