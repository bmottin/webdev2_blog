<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Form\ForgottenPasswordType;
use App\Form\RecoveryPasswordType;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'app_security_login')]
    public function appSecurityLogin(AuthenticationUtils $au): Response
    {
        $lastUsername = $au->getLastUsername();
        $error = $au->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'error' => $error,
            'lastUsername' => $lastUsername
        ]);
    }

    #[Route('/security/logout', name: 'app_security_logout')]
    public function appSecurityLogout()
    {
    }

    #[Route('/security/registration', name: 'app_security_register')]
    public function appSecurityRegister(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $hasher): Response
    {
        
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // je recupere le pass d'origine en clair
            $pass2hash = $user->getPassword();
            // je le hash avec hashPassword
            $pass = $hasher->hashPassword($user, $pass2hash);
            // et j'écrase le mot de passe en clair
            $user->setPassword($pass);

            $user->setRoles(['ROLE_USER']);
            $user->setIsValidate(false);
            $user->setValidationCode($this->generateValidationCode()); // TODO generate validation Code
            $user->setRgpdValidationDate(new \DateTimeImmutable());
            $user->setDateUpdatedAt(new \DateTimeImmutable());

            $userProfile = new UserProfile();
            $userProfile->setDateUpdatedAt(new \DateTimeImmutable());
            $userProfile->setUser($user);

            $entityManager->persist($user);
            $entityManager->persist($userProfile);
            $entityManager->flush();

            $this->forward(
                'App\Controller\MailController::sendRegisterCode',
                [
                    'mail' => $user->getEmail()
                ]
            );
            $this->addFlash("success", "Vous êtes inscrit, vous allez recevoir un mail de confirmation");
            return $this->redirectToRoute('app_main');
        }
        return $this->render('security/registration.html.twig', [
            "form" => $form->createView()
        ]);
    }

    #[Route('/security/validation-code/{code}', name: 'app_security_validation_code')]
    public function appSecurityValidationCode($code, UserRepository $userRepo, EntityManagerInterface $manager)
    {

        $user = $userRepo->findOneBy(['validationCode' => $code]);
        // dd($user);
        if ($user) {
            $user->setIsValidate(true);
            $user->setDateUpdatedAt(new \DateTimeImmutable());
            $manager->persist($user);
            $manager->flush();
            // affichage message succes
            $this->addFlash('success', "Votre compte est validé, veuillez vous connecter.");
            // redirection page login
            return $this->redirectToRoute('app_security_login');
        } else {
            $this->addFlash('warning', "Un probleme est survenu, veuillez renouveller la demande depuis votre fiche profil <a href=''>ici</a>");

            return $this->redirectToRoute('app_security_login');
        }
    }

    #[Route('/security/change-password/{validationCode}', name: 'app_security_change_password')]
    public function appSecurityChangePassword($validationCode, Request $request, UserRepository $userRepository, UserPasswordHasherInterface $hasher): Response
    {
        $user = $userRepository->findOneBy(['validationCode' => $validationCode]);
        $form = $this->createForm(RecoveryPasswordType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pass = $user->getPassword();
            $user->setPassword($hasher->hashPassword($user, $pass));
            $userRepository->save($user, true);
            $this->addFlash('success', "Mot de passe mis à jour avec Succès");
            if($this->getUser()){
                // if connected go to profile
                return $this->redirectToRoute('app_user_show_profile', ['id' => $user->getId()]);
            } else {
                // not connected go to login form
                return $this->redirectToRoute('app_security_login');
            }
            
        }

        if ($user) {

            return $this->render(
                'security/new_password.html.twig',
                [
                    'form' => $form->createView()
                ]
            );
        } else {
            $this->addFlash("danger", "Opération interdite");

            return $this->redirectToRoute("app_main");
        }
    }

    #[Route('/security/forgotten-password', name: 'app_security_forgotten_password')]
    public function appSecurityForgottenPassword(Request $request, UserRepository $userRepository): Response
    {
        // dd($request);
        $userToFind = new User();
        $form = $this->createForm(ForgottenPasswordType::class, $userToFind);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO faire les traitements
            // dd($form);
            // vérifier adresse mail BDD
            $user = $userRepository->findOneBy(['email' => $userToFind->getEmail()]);

            // dd($user);
            // si BDD
            if ($user) { // si c'est false ou null il passe dans le else
                //      alors on envoie un mail  
                // on branche ma route avec mailController 
                $this->forward(
                    'App\Controller\MailController::sendRecoveryPassword',
                    [
                        'mail' => $user->getEmail()
                    ]
                );
            } else {
                // sinon
                //      notification "adresse non existante"
                $this->addFlash("warning", "adresse non existante, vérifiez l'orthographe");
            }
        }

        return $this->render('security/forgotten_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    static function generateValidationCode()
    {

        return md5(uniqid());
    }
}
