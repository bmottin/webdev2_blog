<?php

namespace App\Form;

use App\Entity\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                "label" => "Email, qui servira de login",
                "attr" => [
                    "placeholder" => "benkenobi@sw.com"
                ]
            ])
            ->add('username', TextType::class, [
                "label" => "Pseudo"
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passes doivent correspondres',
                'required' => true, 
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmation Mot de passe']
            ])
            ->add('hasRGPD', CheckboxType::class, [
                'label' => "Acceptez-vous notre politique de confidentialité ?"
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Inscription'
            ])            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
