<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Article1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('tagwords', EntityType::class, [
                'class' => 'App\Entity\Tagword', // namespace des tagword
                'choice_label' => 'label',
                'multiple' => true,
                'expanded' => true,
                'by_reference' => false
            ])
            ->add('content')
            ->add('image')
            ->add('author', EntityType::class, [
                'class' => 'App\Entity\User',
                'choice_label' => 'username',
                'multiple' => false,
                'expanded' => false
            ])
            ->add('category');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
